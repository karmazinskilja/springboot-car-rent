package ua.ithillel.springboot.car.rent.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ua.ithillel.springboot.car.rent.entity.Car;
import ua.ithillel.springboot.car.rent.entity.Order;
import ua.ithillel.springboot.car.rent.repository.CarRepository;
import ua.ithillel.springboot.car.rent.service.CarService;
import ua.ithillel.springboot.car.rent.service.OrderService;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private CarRepository carRepository;

    @Value("${welcome.message}")
    private String message;

    @Autowired
    private CarService carService;


    @GetMapping("/index")
    public String index(Model model) {
        List<Car> cars = carService.getAll();
        model.addAttribute("message", message);
        model.addAttribute("cars", cars);
        return "index";
    }

    @PostMapping("/orders/create/{id}")
    public String createOrder(@PathVariable("id") int carId, HttpSession session) {
        Order order = new Order();
        if (carRepository.findById(carId).isPresent()) {
            order.setCar(carRepository.findById(carId).get());
            carRepository.findById(carId).get().setAvailable(false);
        }
        orderService.createOrder(session, order);
        return "redirect:/index";
    }
}
