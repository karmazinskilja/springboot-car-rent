package ua.ithillel.springboot.car.rent.entity;

import java.io.Serializable;

public enum Role implements Serializable {
    ADMIN(1),
    MANAGER(2),
    CLIENT(3);

    private final int value;
    Role(int value){
        this.value = value;
    }
}
