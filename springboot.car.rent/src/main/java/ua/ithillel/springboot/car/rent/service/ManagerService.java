package ua.ithillel.springboot.car.rent.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.ithillel.springboot.car.rent.entity.Manager;
import ua.ithillel.springboot.car.rent.repository.ManagerRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class ManagerService {

    @Autowired
    private ManagerRepository managerRepository;

    public List<Manager> getAll() {
        List<Manager> managers = new ArrayList<>();
        managerRepository.findAll().forEach(managers::add);
        return managers;
    }
}
