package ua.ithillel.springboot.car.rent.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ithillel.springboot.car.rent.entity.Car;
import ua.ithillel.springboot.car.rent.repository.CarRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class CarService {

    @Autowired
    private CarRepository carRepository;

    public List<Car> getAll() {
        List<Car> cars = new ArrayList<>();
        carRepository.findAll().forEach(cars::add);
        return cars;
    }

    @Transactional
    public void createCar(Car car) {
        if (!getAll().isEmpty()) {
            int maxId = getAll().stream()
                    .map(Car::getId)
                    .mapToInt(value -> value)
                    .max()
                    .orElseThrow();

            car.setId(maxId + 1);
        } else car.setId(1);

        car.setAvailable(true);
        carRepository.save(car);
    }

    @Transactional
    public void deleteCar(int id) {
        carRepository.deleteById(id);
    }
}
