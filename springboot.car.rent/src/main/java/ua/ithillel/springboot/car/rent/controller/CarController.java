package ua.ithillel.springboot.car.rent.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ua.ithillel.springboot.car.rent.entity.Car;
import ua.ithillel.springboot.car.rent.service.CarService;

@Controller
@RequestMapping("/cars")
public class CarController {

    @Autowired
    private CarService carService;

    @RequestMapping("/add")
    public String addCar(Model model) {
        model.addAttribute("car", new Car());
        return "add-car";
    }

    @PostMapping("/create")
    public String createCar(@ModelAttribute("car") Car car) {
        carService.createCar(car);
        return "redirect:/index";
    }

    @PostMapping("/delete/{id}")
    public String deleteCar(@PathVariable("id") int id) {
        carService.deleteCar(id);
        return "redirect:/index";
    }

}
