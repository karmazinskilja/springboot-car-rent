package ua.ithillel.springboot.car.rent.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ua.ithillel.springboot.car.rent.params.LoginError;
import ua.ithillel.springboot.car.rent.request.LoginRequest;
import ua.ithillel.springboot.car.rent.entity.Client;
import ua.ithillel.springboot.car.rent.entity.Manager;
import ua.ithillel.springboot.car.rent.service.ClientService;
import ua.ithillel.springboot.car.rent.service.ManagerService;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Objects;

@Controller
public class LoginController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private ManagerService managerService;

    @RequestMapping("/")
    public String showLogin(Model model) {
        LoginError loginError = new LoginError();
        loginError.setError(false);
        model.addAttribute("loginError", loginError);
        return "/login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@ModelAttribute(name = "loginRequest") LoginRequest loginRequest, Model model,
                        HttpSession session) {

        String login = loginRequest.getLogin();
        String password = loginRequest.getPassword();
        boolean isManager = loginRequest.isManager();

        if (!isManager) {
            List<Client> clients = clientService.getAll();
            for (Client client : clients) {
                if (Objects.equals(client.getLogin(), login) && Objects.equals(client.getPassword(), password)) {

                    session.setAttribute("isManager", false);
                    session.setAttribute("id", client.getId());

                    return "redirect:/index";
                }
            }
        } else {
            List<Manager> managers = managerService.getAll();
            for (Manager manager : managers) {
                if (Objects.equals(manager.getLogin(), login) && Objects.equals(manager.getPassword(), password)) {

                    session.setAttribute("isManager", true);
                    session.setAttribute("id", manager.getId());

                    return "redirect:/index";
                }
            }
        }
        LoginError loginError = new LoginError();
        loginError.setError(true);
        model.addAttribute("loginError", loginError);
        return "/login";
    }
}
