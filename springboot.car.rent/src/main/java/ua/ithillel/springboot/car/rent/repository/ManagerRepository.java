package ua.ithillel.springboot.car.rent.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.ithillel.springboot.car.rent.entity.Manager;

@Repository
public interface ManagerRepository extends CrudRepository<Manager, Integer> {
}
