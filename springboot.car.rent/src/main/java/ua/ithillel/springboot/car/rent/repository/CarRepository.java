package ua.ithillel.springboot.car.rent.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.ithillel.springboot.car.rent.entity.Car;

@Repository
public interface CarRepository extends CrudRepository<Car, Integer> {

}
