package ua.ithillel.springboot.car.rent.params;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginError {
    private boolean error;
}
