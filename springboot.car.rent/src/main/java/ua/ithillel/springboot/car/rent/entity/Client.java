package ua.ithillel.springboot.car.rent.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "clients")
@ToString
public class Client implements Serializable {

    @Id
    @Column(name = "id")
    private int id;
    private String name;
    private String surname;
    private String phone;
    private String login;
    private String password;

}
