package ua.ithillel.springboot.car.rent.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "managers")
@ToString
public class Manager implements Serializable {

    @Id
    private int id;
    private String name;
    private String login;
    private String password;
    private String email;

    @Enumerated
    private Role role;

}
