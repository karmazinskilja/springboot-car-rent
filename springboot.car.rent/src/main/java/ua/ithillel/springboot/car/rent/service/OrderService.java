package ua.ithillel.springboot.car.rent.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.ithillel.springboot.car.rent.entity.Client;
import ua.ithillel.springboot.car.rent.entity.Manager;
import ua.ithillel.springboot.car.rent.entity.Order;
import ua.ithillel.springboot.car.rent.repository.ClientRepository;
import ua.ithillel.springboot.car.rent.repository.OrderRepository;

import javax.servlet.http.HttpSession;
import java.util.*;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private ManagerService managerService;

    public List<Order> getAll() {
        List<Order> orders = new ArrayList<>();
        orderRepository.findAll().forEach(orders::add);
        return orders;
    }

    @Transactional
    public void deleteOrder(int id) {
        orderRepository.deleteById(id);
    }

    @Transactional
    public void setCarAvailable(int id) {
        if (orderRepository.findById(id).isPresent()) {
            Order toRemove = orderRepository.findById(id).get();
            toRemove.getCar().setAvailable(true);
        }
    }

    @Transactional
    public void createOrder(HttpSession session, Order order) {
        if (!getAll().isEmpty()) {
            int maxId = getAll().stream()
                    .map(Order::getId)
                    .mapToInt(value -> value)
                    .max()
                    .orElseThrow();
            order.setId(maxId + 1);
        } else order.setId(1);

        order.setDate(new Date());
        if (clientRepository.findById((int) session.getAttribute("id")).isPresent()) {
            Client aClient = clientRepository.findById((int) session.getAttribute("id")).get();
            order.setClient(aClient);
        }

        List<Manager> managers = managerService.getAll();
        Random rand = new Random();
        order.setManager(managers.get(rand.nextInt(managers.size())));
        orderRepository.save(order);
    }
}
