package ua.ithillel.springboot.car.rent.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.ithillel.springboot.car.rent.entity.Client;

@Repository
public interface ClientRepository extends CrudRepository<Client, Integer> {

    Client findByLoginAndPassword(String login, String password);
}
