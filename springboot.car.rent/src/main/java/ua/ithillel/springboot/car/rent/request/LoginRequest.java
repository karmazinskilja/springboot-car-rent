package ua.ithillel.springboot.car.rent.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequest {

    private String login;
    private String password;
    private boolean manager;
}
