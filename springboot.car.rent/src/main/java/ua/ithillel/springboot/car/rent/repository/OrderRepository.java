package ua.ithillel.springboot.car.rent.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.ithillel.springboot.car.rent.entity.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order, Integer> {
}
