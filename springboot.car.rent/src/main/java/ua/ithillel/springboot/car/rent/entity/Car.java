package ua.ithillel.springboot.car.rent.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "cars")
public class Car implements Serializable {

    @Id
    @Column(name = "id")
    private int id;
    private String manufacturer;
    private String model;
    private int year;
    private int price;
    private boolean available;

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", price=" + price +
                ", available=" + available +
                '}';
    }
}
